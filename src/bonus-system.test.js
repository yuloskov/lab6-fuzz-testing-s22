import { calculateBonuses } from "./bonus-system.js";

describe("Bonus system test", () => {
  const precision = 5;
  console.log("Tests started");

  test("Standard bonus program", () => {
    const program = "Standard";
    expect(calculateBonuses(program, 100)).toBeCloseTo(0.05, precision);
    expect(calculateBonuses(program, 20000)).toBeCloseTo(0.075, precision);
    expect(calculateBonuses(program, 55000)).toBeCloseTo(0.1, precision);
    expect(calculateBonuses(program, 104000)).toBeCloseTo(0.125, precision);
  });

  test("Premium bonus program", () => {
    const program = "Premium";
    expect(calculateBonuses(program, 100)).toBeCloseTo(0.1, precision);
    expect(calculateBonuses(program, 20000)).toBeCloseTo(0.15, precision);
    expect(calculateBonuses(program, 55000)).toBeCloseTo(0.2, precision);
    expect(calculateBonuses(program, 104000)).toBeCloseTo(0.25, precision);
  });

  test("Diamond bonus program", () => {
    const program = "Diamond";
    expect(calculateBonuses(program, 100)).toBeCloseTo(0.2, precision);
    expect(calculateBonuses(program, 20000)).toBeCloseTo(0.3, precision);
    expect(calculateBonuses(program, 55000)).toBeCloseTo(0.4, precision);
    expect(calculateBonuses(program, 104000)).toBeCloseTo(0.5, precision);
  });

  test("Invalid program test", () => {
    const program = "ultraGigaDiamond";
    expect(calculateBonuses(program, 100)).toBeCloseTo(0, precision);
    expect(calculateBonuses(program, 20000)).toBeCloseTo(0, precision);
    expect(calculateBonuses(program, 55000)).toBeCloseTo(0, precision);
    expect(calculateBonuses(program, 104000)).toBeCloseTo(0, precision);
  });

  console.log("Tests Finished");
});
